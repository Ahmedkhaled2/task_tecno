<?php

namespace App\Helper;

use App\Client;
use App\PaymentGetWay;
use App\Reservation;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;

/**
 * Class Rates
 * @package App\Helpers
 */
class Rates
{
    /**
     * @return array|mixed|\stdClass
     */
    public static function geoLocate()
    {

        $response = Curl::to('https://api.vatcomply.com/geolocate')
            ->asJson()
            ->get();

        return $response;
    }

    /**
     * @return array|mixed|\stdClass
     */
    public static function rateBaseOnLocation($base = null, $date = null)
    {
        $current = self::geolocate()->currency ?? null;

        $response = Curl::to('https://api.vatcomply.com/rates')
            ->withData([
                'base' => $base ?? 'USD',
                'date' => $date,
            ])
            ->asJson()
            ->get();

        return $response;
    }

}
