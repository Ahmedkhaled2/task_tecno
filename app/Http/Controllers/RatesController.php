<?php

namespace App\Http\Controllers;

use App\Helper\Rates;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 *
 */
class RatesController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): view
    {
        $rates = Rates::rateBaseOnLocation($request->base, $request->date);

        return view('index', compact('rates'));
    }

}
