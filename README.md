## About Task

Website page for currencies rate with filter by currency and date using VAT comply Api
using curl to call Api
- [VAT comply documentation](https://www.vatcomply.com/documentation).

## Technology

Metronic dashboard (Html,Css,js)
Laravel framework .

## Libraries

[ixudra/curl](https://github.com/ixudra/curl).

### Run App

- Clone project
- Run composer install
- php artisan serve
- Open [localhost](http://localhost:8000/)

## Record 

- [Video](https://www.loom.com/share/964202a1851840b29580be7b11553921)
