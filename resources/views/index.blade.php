@extends('layouts.master')
@section('styles')
@endsection
@section('subheader')
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-2">
                <!--begin::Page Title-->
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Currency</h5>
                <!--end::Page Title-->
            </div>
            <!--end::Info-->
        </div>
    </div>
@endsection
@section('content')

    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form action="{{route('index')}}" method="get">
                            {!! csrf_field() !!}
                            <div class="card-body row">
                                <div class="col-xl-6 ">
                                    <select class="form-control m-input" name="base">
                                        <option value="">Currencies</option>
                                        @foreach($rates->rates as $key => $rate)
                                            <option
                                                value="{{$key}}" {{$rates->base==$key?'selected':null}}>{{$key}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xl-6 ">
                                    <input class="form-control m-input" type="date"
                                           placeholder="Date" value="{{$rates->date}}"
                                           name="date" max="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
                                </div>
                                <div class="col-xl-12 mt-5">
                                    <button class="btn btn-success col-md-12" type="submit">
                                        Search
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="col-xl-6 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h1>Bate:{{$rates->date}}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 mt-5">
                    <div class="card">
                        <div class="card-body">
                            <h1>Base:{{$rates->base}}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 mt-5">
                    <div class="row">
                        @foreach($rates->rates as$key => $rate)
                            <div class="col-xl-3">
                                <!--begin::Stats Widget 29-->
                                <div class="card card-custom bgi-no-repeat card-stretch gutter-b"
                                     style="background-position: right top; background-size: 30% auto;
                                         height: calc(80% - 25px);
                                         background-image: url({{asset('assets/media/svg/shapes/abstract-1.svg')}})">
                                    <!--begin::Body-->
                                    <div class="card-body">
                                    <span
                                        class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{$key}} :{{round($rate)}}</span>
                                    </div>
                                    <!--end::Body-->
                                </div>
                                <!--end::Stats Widget 29-->
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!--end::Container-->
    </div>
@endsection
